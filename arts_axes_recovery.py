
from matplotlib import projections
import numpy as np
import os
import math
# import csv
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
# from matplotlib import projections
import quaternion
import matplotlib.animation as animation


# Get all recorded data from CSV file
# for f in os.Dirpath:



rows = np.genfromtxt('data/ROLL/MT_03881073-014-000.txt', skip_header=13, delimiter=',')
rows = np.delete(rows, [0,1], axis=1)

qs = quaternion.as_quat_array(rows)

qf = quaternion.as_float_array(qs)
plt.figure()
plt.plot(qf)
plt.show(False)

# Find the relevant index and retrieve an existed calibration quaternions
print('Choose STRAIGHT, STRAIGHT_ROLLED, BEND1, BEND2, BEND3, START, STOP')
# ixs = plt.ginput(5)
# ixs = np.floor(ixs)[:,0]
# ixs = ixs.astype('int')

# Section for roll in straight less than 360[deg]!
# Straight, Straight+rolled, Art1, Art2, Art3, Start, Stop
ixs = np.array([300, 400, 1182, 2703, 4493,  300, 5541]) # STARIGHT rolled
# ixs = np.array([1260,1320]) # BENT rolled

ixs = ixs.astype('int')

# Projected quaternion for all measruements (refereneced to ART1 bend)
qs_t = qs/qs[ixs[0]]
vecs_t = quaternion.as_rotation_vector(qs_t)

plt.figure()
ax = plt.axes(projection='3d')
ax.autoscale()

Z=np.zeros(len(vecs_t))
jp=np.arange(0,len(Z),50)
jp=[253,1195,2775,4544]
plt.quiver(Z[jp],Z[jp],Z[jp],vecs_t[(jp,0)],vecs_t[(jp,1)],vecs_t[(jp,2)],edgecolor='r',linewidth = 1)

ax.set_xlim([-1.5,1.5])
ax.set_ylim([-1.5,1.5])
ax.set_zlim([-1.5,1.5])
ax.set_aspect('equal','box')
plt.show(False)



jp = np.arange(ixs[0],ixs[1])

# Average for all rotation axes
vecs_t = vecs_t[jp]
vecs_t = vecs_t[np.linalg.norm(vecs_t,axis=1)>math.pi/3]

vecs_t_len = np.linalg.norm(vecs_t,axis=1)
vecs_t_len = np.reshape(vecs_t_len,(-1,1))
vecs_tn = (vecs_t)/vecs_t_len

roll_axis_vec = np.mean(vecs_tn,axis=0)

Z=np.zeros(len(vecs_t))
plt.figure()
ax = plt.axes(projection='3d')
ax.autoscale()
plt.quiver(Z,Z,Z,vecs_tn[:,0],vecs_tn[:,1],vecs_tn[:,2],edgecolor='r')
plt.quiver(0,0,0,roll_axis_vec[0],roll_axis_vec[1],roll_axis_vec[2],edgecolor='b')
ax.set_aspect('equal','box')
plt.show(False)

print()