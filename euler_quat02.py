
from matplotlib import projections
import numpy as np
import os
import math
# import csv
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
# from matplotlib import projections
import quaternion
import matplotlib.animation as animation


# Get all recorded data from CSV file
# for f in os.Dirpath:

    

rows = np.genfromtxt('data/ROLL/MT_03881073-014-000.txt', skip_header=13, delimiter=',')
rows = np.delete(rows, [0,1], axis=1)

print(rows)

# np.quaternion(1,0,0,0)
qs = quaternion.as_quat_array(rows)

# plotting the points
# plt.plot(quaternion.as_euler_angles(qs))
qf = quaternion.as_float_array(qs)
# h = plt.figure()
# plt.figure()
plt.plot(qf[:,0])
plt.plot(qf[:,1])
plt.plot(qf[:,2])
plt.plot(qf[:,3])
 
# function to show the plot
plt.show(False)
# Selecting the relevant quaternions from the data:
# 1. STRAIGHT
# 2. BEND towards ART1
# 3. BEND towards ART2
# 4. BEND towards ART3

# Find the relevant index and retrieve an existed calibration quaternions
print('Choose STRAIGHT, BEND1, BEND2, BEND3, START, STOP')
# ixs = plt.ginput(12)
# ixs = np.floor(ixs)[:,0]
ixs = np.array([300,400])
ixs = ixs.astype('int')

# ixs = np.array([ 493,  682,  904, 1149])

print(ixs)

# qcal = quaternio
qcal = qs[ixs]
# q = quaternion(1,0,0,0)
# q.angular_velocity()


qcal_t = qcal/qcal[0]
# qcal_t = qcal
vecs = quaternion.as_rotation_vector(qcal_t)

plt.figure(2)
ax = plt.axes(projection='3d')
ax.autoscale()

# # plt.quiver(Z, vecs[0], color=['r','b','g'], scale=21)
# plt.quiver(0,0,0,vecs[0][0],vecs[0][1],vecs[0][2])
# plt.quiver(0,0,0,vecs[1][0],vecs[1][1],vecs[1][2])
# plt.quiver(0,0,0,vecs[2][0],vecs[2][1],vecs[2][2])

# Z=np.zeros(len(vecs))
# jp=np.arange(0,len(Z))
# jp1 = np.arange(0,6)
# jp2 = np.arange(6,12)
# plt.quiver(Z[jp1],Z[jp1],Z[jp1],vecs[(jp1,0)],vecs[(jp1,1)],vecs[(jp1,2)],edgecolor='r',linewidth = 8)
# plt.quiver(Z[jp2],Z[jp2],Z[jp2],vecs[(jp2,0)],vecs[(jp2,1)],vecs[(jp2,2)],edgecolor='b',linewidth = 8)


ax.set_xlim([-1.5,1.5])
ax.set_ylim([-1.5,1.5])
ax.set_zlim([-1.5,1.5])
ax.set_aspect('equal','box')
# ax.autoscale(True)

plt.show(False)

# Projected quaternion for all measruements (refereneced to ART1 bend)
qs_t = qs/qs[ixs[0]]
vecs_t = quaternion.as_rotation_vector(qs_t)


# Print decimated recorded rotation vectors
Z=np.zeros(len(vecs_t))
# jp = np.arange(1,len(vecs_t),200)
jp = np.arange(ixs[0],ixs[1])
# jp = np.arange(4493,4685)


plt.figure(5)
ax = plt.axes(projection='3d')
ax.autoscale()
plt.quiver(Z[jp],Z[jp],Z[jp],vecs_t[(jp,0)],vecs_t[(jp,1)],vecs_t[(jp,2)],edgecolor='r')
ax.set_aspect('equal','box')
plt.show(False)

plt.figure()
plt.plot(np.linalg.norm(vecs_t[jp],axis=1))
plt.show(False)






# Average for all roatation axes
vecs_t = vecs_t[jp]
vecs_t = vecs_t[np.linalg.norm(vecs_t,axis=1)>math.pi/6]

vecs_t_len = np.linalg.norm(vecs_t,axis=1)
vecs_t_len = np.reshape(vecs_t_len,(-1,1))
vecs_tn = (vecs_t)/vecs_t_len

roll_axis_vec = np.mean(vecs_tn,axis=0)

Z=np.zeros(len(vecs_t))
plt.figure()
ax = plt.axes(projection='3d')
ax.autoscale()
plt.quiver(Z,Z,Z,vecs_tn[:,0],vecs_tn[:,1],vecs_tn[:,2],edgecolor='r')
plt.quiver(0,0,0,roll_axis_vec[0],roll_axis_vec[1],roll_axis_vec[2],edgecolor='b')
ax.set_aspect('equal','box')
plt.show(False)


def data_gen(i):
    ax.cla()
    ax.quiver(0,0,0,vecs_t[i][0],vecs_t[i][1],vecs_t[i][2])
    # v = np.random.random(3)
    # ax.quiver(0,0,0,v[0],v[1],v[2])
    
    ax.set_xlim(-3, 3)
    ax.set_ylim(-3, 3)
    ax.set_zlim(-3, 3)
    ax.view_init(elev=30, azim=60)
    print(i)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
# plt.show(False)
data_gen(0)
# ani = animation.FuncAnimation(fig, data_gen, range(0,len(vecs_t),3), blit=False,interval=30)
ani = animation.FuncAnimation(fig, data_gen, jp, blit=False, interval=10)
plt.show(True)


angs = np.linalg.norm(vecs_t,axis=1)*180/math.pi

# jp=2001
# Recover angles between each articulation cable (direction per art)
art1_n = (vecs[2])/np.linalg.norm(vecs[2])
art2_n = (vecs[1])/np.linalg.norm(vecs[1])
art3_n = (vecs[0])/np.linalg.norm(vecs[0])


# mv_n = (vecs_t[jp])/np.linalg.norm(vecs_t[jp])
# dot = np.dot(art1_n,mv_n)
# angle = np. arccos(dot) * 180.0/math.pi

# plt.quiver(Z[jp],Z[jp],Z[jp],vecs_t[(jp,0)],vecs_t[(jp,1)],vecs_t[(jp,2)],edgecolor='g',linewidth = 10)

mv_len = np.linalg.norm(vecs_t,axis=1)
mv_len = np.reshape(mv_len,(-1,1))
mv_n = (vecs_t)/mv_len

dot1 = np.dot(mv_n,art1_n)
angle1 = np. arccos(dot1) * 180.0/math.pi

dot2 = np.dot(mv_n,art2_n)
angle2 = np. arccos(dot2) * 180.0/math.pi

dot3 = np.dot(mv_n,art3_n)
angle3 = np. arccos(dot3) * 180.0/math.pi

fig = plt.figure()
angs_mat = np.append(angle1.reshape((-1,1)),angle2.reshape((-1,1)), axis=1)
angs_mat = np.append(angs_mat, angle3.reshape((-1,1)), axis=1)

plt.plot(angs_mat)
plt.show(False)


bend = mv_len*180.0/math.pi

plt.figure()
plt.plot(angle1,bend,label='dir1')
plt.plot(angle2,bend,label='dir2')
plt.plot(angle3,bend,label='dir3')
plt.legend()
plt.grid()
plt.show(False)


# Reduce to 0-90[deg]

# delta error to reach constant bend

roi = np.arange(ixs[4],ixs[5])
inds = np.argwhere(angle2<=60)

roi = np.intersect1d(roi,inds)

plt.figure()
plt.plot(angle2[roi],bend[roi])
plt.grid()
plt.show(False)

     
def print_tip_orientation(quat):
    quat = quaternion.from_float_array(quat)
    quat_t = quat/qcal[0]
    vecs_t = quaternion.as_rotation_vector(quat_t)

    # mv_len = np.linalg.norm(vecs_t,axis=1)
    # mv_len = np.reshape(mv_len,(-1,1))
    mv_len = np.linalg.norm(vecs_t)
    mv_n = (vecs_t)/mv_len

    dot1 = np.dot(mv_n,art1_n)
    angle1 = np.arccos(dot1) * 180.0/math.pi

    dot2 = np.dot(mv_n,art2_n)
    angle2 = np.arccos(dot2) * 180.0/math.pi

    dot3 = np.dot(mv_n,art3_n)
    angle3 = np.arccos(dot3) * 180.0/math.pi

    bend = mv_len*180.0/math.pi

    print("bend: ", f"{bend:.1f}","dir1: ", f"{angle1:.1f}", "dir2: ", f"{angle2:.1f}", "dir3: ", f"{angle3:.1f}")













# print()









# Compute reference axis (RIGHT vector) to "offset" the output DIRECTION axis against
# 1. 


# Compute and output BEND+DIRECTION+ROLL for all data points
# 1. Remove roll (convert to euler angles + zero roll + convert back to quaternion)
# 2. Convert to axis-angle projection
# 3. Project the axis according to the calibrated frame


# Calc error (bend ripples, nonlinear deltas, etc.)




# Convert from quaternion to euler angles
def euler_from_quaternion(x, y, z, w):
    """
    Convert a quaternion into euler angles (roll, pitch, yaw)
    roll is rotation around x in radians (counterclockwise)
    pitch is rotation around y in radians (counterclockwise)
    yaw is rotation around z in radians (counterclockwise)
    """
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = math.atan2(t0, t1)
    
    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = math.asin(t2)
    
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = math.atan2(t3, t4)
    
    return roll_x, pitch_y, yaw_z # in radians


# a=np.array([0.0678,0.0678,0.0533,0.0344,-0.0267,-0.0544,-0.0544])
# a = a*75/90

# print(a)


# b = np.array([-0.005946901269245122,
#   -0.0032211059347523195,
#   -0.0038440699902768383,
#   -0.004335185893314178,
#   -0.004618082667223961,
#   -0.004610544806595846,
#   -0.004229573289430787,
#   -0.0033917453968697483,
#   -0.002028051041620645,
#   -0.0000779737869300087,
#   0.0024926916286312248,
#   0.00569982765877049,
#   0.009534812572960348,
#   0.013961833701309628,
#   0.018917781140524174,
#   0.024309521855601125,
#   0.030016610101736976,
#   0.03589781173597167,
#   0.04180040186066688,
#   0.047555344979786536,
#   0.05298415762863876,
#   0.05792015092713824,
#   0.06221396733171683,
#   0.06570516590449127,
#   0.06829411291109369,
#   0.069881349050518,
#   0.07041715978547827,
#   0.069881349050518,
#   0.06829411291109369,
#   0.06570516590449127,
#   0.06221396733171683,
#   0.05792015092713824,
#   0.05298415762863876,
#   0.047555344979786536,
#   0.04180040186066688,
#   0.03589781173597167,
#   0.030016610101736976,
#   0.024309521855601125,
#   0.018917781140524174,
#   0.013961833701309628,
#   0.009534812572960348,
#   0.00569982765877049,
#   0.0024926916286312248,
#   -0.0000779737869300087,
#   -0.002028051041620645,
#   -0.0033917453968697483,
#   -0.004229573289430787,
#   -0.004610544806595846,
#   -0.004618082667223961,
#   -0.004335185893314178,
#   -0.0038440699902768383,
#   -0.0032211059347523195,
#   -0.005946901269245122])


# #   print(b)
# print(b.shape)
# b=b/b.sum()



# print(b)