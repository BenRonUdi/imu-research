
from matplotlib import projections
from matplotlib.transforms import Transform
import numpy as np
import os
import math
# import csv
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
# from matplotlib import projections
import quaternion
import matplotlib.animation as animation
from scipy.spatial import *
import tkinter as tk

# Get all recorded data from CSV file
# for f in os.Dirpath:

def plotquat(ax, q, qcolor, name):
    plt.quiver(0,0,0,q[0],q[1],q[2],color=qcolor)
    ax.text(q[0],q[1],q[2],s=name)

    
angle=2/3*math.pi
rot_angle = np.array([0,angle,0])


x0 = [1,0,0]
x1 = np.cross(x0,rot_angle)
x2 = np.cross(x0,-rot_angle)


# rows = np.genfromtxt('data/ROLL/MT_03881073-014-000.txt', skip_header=13, delimiter=',')
rows = np.genfromtxt('data/V2/MT_03881073-019-000.txt', skip_header=13, delimiter=',')

rows = np.delete(rows, [0,1], axis=1)


qm = quaternion.as_quat_array(rows)

# qf = quaternion.as_float_array(qm)
# plt.figure()
# plt.plot(qf)
# plt.show(False)


# Section for roll in straight less than 360[deg]!
print('Choose STRAIGHT, STRAIGHT_ROLLED, BEND1, BEND2, BEND3')
# ixs = plt.ginput(5)
# ixs = np.floor(ixs)[:,0]
# ixs = ixs.astype('int')

# Section for roll in straight less than 360[deg]!
# Startight, Straight+rolled, Art1, Art2, Art3
# ixs = np.array([300, 400, 1182, 2703, 4493,  300, 5541])
ixs = np.array([1803, 2218, 2540, 3197, 4297])
ixs = ixs.astype('int')

qms1 = qm[ixs[0]]
qms2 = qm[ixs[1]]

qmbend1 = qm[ixs[2]]
qmbend2 = qm[ixs[3]]
qmbend3 = qm[ixs[4]]



# Print all X axes of all measured orientations
x0 = [1,0,0]
xms1 = quaternion.rotate_vectors(qms1,x0)
xms2 = quaternion.rotate_vectors(qms2,x0)
xmbend1 = quaternion.rotate_vectors(qmbend1,x0)
xmbend2 = quaternion.rotate_vectors(qmbend2,x0)
xmbend3 = quaternion.rotate_vectors(qmbend3,x0)



plt.figure()
ax = plt.axes(projection='3d')

plt.quiver(0,0,0,xms1[0],xms1[1],xms1[2],color='r')
plt.quiver(0,0,0,xms2[0],xms2[1],xms2[2],color='r',linewidth=2)
plt.quiver(0,0,0,xmbend1[0],xmbend1[1],xmbend1[2],color='b')
plt.quiver(0,0,0,xmbend2[0],xmbend2[1],xmbend2[2],color='b')
plt.quiver(0,0,0,xmbend3[0],xmbend3[1],xmbend3[2],color='b')

ax.set_aspect('equal','box')
lim=1
ax.set_xlim3d([-lim,lim])
ax.set_ylim3d([-lim,lim])
ax.set_zlim3d([-lim,lim])
 
plt.show(False)









# Shaft roll - recovery of true shaft X axis
dq_roll = qms1 * qms2.inverse()
vroll = quaternion.as_rotation_vector(dq_roll)
vroll = vroll/np.linalg.norm(vroll)

# Recovery of bend1 rotation axis vector
dq_bend1 = qmbend1 * qms2.inverse()
vbend1 = quaternion.as_rotation_vector(dq_bend1)
dq_bend2 = qmbend2 * qms2.inverse()
vbend2 = quaternion.as_rotation_vector(dq_bend2)
dq_bend3 = qmbend3 * qms2.inverse()
vbend3 = quaternion.as_rotation_vector(dq_bend3)

vbend1_ort = vbend1 - vroll.dot(vbend1) * vroll
vbend2_ort = vbend2 - vroll.dot(vbend2) * vroll
vbend3_ort = vbend3 - vroll.dot(vbend3) * vroll

vleft_ort = np.cross(vroll,vbend1_ort)

q1 = quaternion.from_rotation_matrix([vroll,vbend1_ort,vleft_ort])

plt.figure()
ax = plt.axes(projection='3d')



plotquat(ax,vroll,'r',"vroll")
plotquat(ax,vbend1_ort,'g',"vbend1_ort")
plotquat(ax,vleft_ort,'b',"vleft_ort")

plotquat(ax,vbend2_ort,'g',"vbend2_ort")
plotquat(ax,vbend3_ort,'g',"vbend3_ort")



ax.set_aspect('equal','box')
lim=1
ax.set_xlim3d([-lim,lim])
ax.set_ylim3d([-lim,lim])
ax.set_zlim3d([-lim,lim])
 
plt.show(False)

# Finally, we find the relevant imu offset
dq = qms2 * q1.inverse()

# Measured quat after alignment to the "true" shaft datum
dqm_t = dq * qm * q1.inverse()
# dqm_t = dq.inverse() * qm


vrots = quaternion.rotate_vectors(dqm_t,x0)

# vrots = quaternion.as_rotation_vector(dqm_t)


Z=np.zeros(len(vrots))
# plt.figure()
# ax = plt.axes(projection='3d')
ax.autoscale()
decim = 10
jp = list(range(1,len(vrots),decim))
plt.quiver(Z[jp],Z[jp],Z[jp],vrots[jp,0],vrots[jp,1],vrots[jp,2],edgecolor='k')
# ax.set_aspect('equal','box')
# ax.set_xlim3d([-2,2])
# ax.set_ylim3d([-2,2])
# ax.set_zlim3d([-2,2])
plt.show(False)





x0 = [1,0,0]
xs = quaternion.rotate_vectors(qshaft,x0)
x1 = quaternion.rotate_vectors(q1,x0)
x2 = quaternion.rotate_vectors(q2,x0)
x3 = quaternion.rotate_vectors(q3,x0)








qm_t = qm/qm[ixs[0]]
vecs_t = quaternion.as_rotation_vector(qm_t)

jp = np.arange(ixs[0],ixs[1])


xspins = quaternion.rotate_vectors(qm,[1,0,0])
# yspins = quaternion.rotate_vectors(qm)
# xspins = xspins(jp)

xstraight = np.mean(xspins)



# ax.scatter3D(vecs[:,0],vecs[:,1],vecs[:,2])

# Average for all roatation axes
vecs_t_selected = vecs_t[jp]
vecs_t_selected = vecs_t_selected[np.linalg.norm(vecs_t_selected,axis=1)>math.pi/6]

vecs_t_len = np.linalg.norm(vecs_t_selected,axis=1)
vecs_t_len = np.reshape(vecs_t_len,(-1,1))
vecs_tn = (vecs_t_selected)/vecs_t_len

roll_axis_vec = np.mean(vecs_tn,axis=0)
roll_axis_vec = roll_axis_vec/np.linalg.norm(roll_axis_vec)

Z=np.zeros(len(vecs_t_selected))
plt.figure()
ax = plt.axes(projection='3d')
ax.autoscale()
# ax.scatter3D(0,0,0)
# ax.scatter3D(vecs_tn[:,0],vecs_tn[:,1],vecs_tn[:,2],edgecolor='r')
# ax.scatter3D(vecs[:,0],vecs[:,1],vecs[:,2])
plt.quiver(Z,Z,Z,vecs_tn[:,0],vecs_tn[:,1],vecs_tn[:,2],edgecolor='r')
plt.quiver(0,0,0,roll_axis_vec[0],roll_axis_vec[1],roll_axis_vec[2],edgecolor='b')
ax.set_aspect('equal','box')
ax.set_xlim3d([-2,2])
ax.set_ylim3d([-2,2])
ax.set_zlim3d([-2,2])
plt.show(False)

# # Here we found the roll_axis_vec (straight axis)
# qstraight = quaternion.as_quat_array([1, roll_axis_vec[0],roll_axis_vec[1],roll_axis_vec[2]])
# qstraight = qstraight.normalized()

# vstraight = quaternion.as_rotation_vector(qstraight)
# vstraight = vstraight/np.linalg.norm(vstraight)

# We need to find the quaternions that truly describes the rotation between ENU-based reference frame and the ideal shaft frame
align_rot_vec = np.cross([1,0,0],roll_axis_vec)
ang = math.asin(np.linalg.norm(align_rot_vec))
align_rot_vec = ang*align_rot_vec/np.linalg.norm(align_rot_vec)
# Convert the rotation vector to quaternion
qstraight_t = quaternion.as_quat_array([1,align_rot_vec[0],align_rot_vec[1],align_rot_vec[2]])
qstraight_t = qstraight_t.normalized()



# Select art1,art2,art3
jp = np.array(ixs[2:5])
qm_t = qm[jp]/qstraight

vecs_t = quaternion.as_rotation_vector(qm_t)
vecs_t_selected = vecs_t

# # Average for all roatation axes
# vecs_t_selected = vecs_t[jp]
# vecs_t_selected = vecs_t_selected[np.linalg.norm(vecs_t_selected,axis=1)>math.pi/6]

# vecs_t_len = np.linalg.norm(vecs_t_selected,axis=1)
# vecs_t_len = np.reshape(vecs_t_len,(-1,1))
# vecs_tn = (vecs_t_selected)/vecs_t_len

# Show all art bends quaternions
Z=np.zeros(len(vecs_t_selected))
plt.figure()
ax = plt.axes(projection='3d')
ax.autoscale()
plt.quiver(Z,Z,Z,vecs_t_selected[:,0],vecs_t_selected[:,1],vecs_t_selected[:,2],edgecolor='g')
ax.set_aspect('equal','box')
plt.show(False)



print()