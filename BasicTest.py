import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.spatial.transform import Rotation as R
import quaternion

angle=1/2*math.pi
cs = math.cos(2/3*math.pi)
sn = math.sin(2/3*math.pi)
rv1 = angle * np.array([0,1,0])
rv2 = angle * np.array([0,cs,sn])
rv3 = angle * np.array([0,cs,-sn])

dv = [np.deg2rad(0),np.deg2rad(15),np.deg2rad(8)]

qshaft = quaternion.from_rotation_vector([np.deg2rad(0),np.deg2rad(137),np.deg2rad(20)]) # Define the "true" shaft frame
# qshaft = quaternion.from_rotation_vector([0,np.deg2rad(0),0]) # Define the "true" shaft frame
# qdelta1 = quaternion.from_rotation_vector([np.deg2rad(5),np.deg2rad(15),np.deg2rad(25)]) # Measured misaligned straight orientation
qdelta1 = quaternion.from_rotation_vector(dv) # Measured misaligned straight orientation (delta from shaft orientation)

# qs = qunit * quaternion.from_rotation_vector([0,0.6,0])
# q0 = qs.inverse() * quaternion.from_rotation_vector() * qs
q1 = qshaft*quaternion.from_rotation_vector(rv1)
q2 = qshaft*quaternion.from_rotation_vector(rv2)
q3 = qshaft*quaternion.from_rotation_vector(rv3)

qsm_rotated = qshaft * quaternion.from_rotation_vector([np.deg2rad(90),0,0])* qdelta1
qsm =  qshaft * qdelta1

# Find the rotation vector that brings us from qsm towards qsm_rotated to find vector shaft
qrot = qsm_rotated * qsm.inverse()
vrot = quaternion.as_rotation_vector(qrot)
print(vrot)
print(np.linalg.norm(vrot))

q1m = qdelta1 * q1
q2m = qdelta1 * q2
q3m = qdelta1 * q3

# q1 = qs.inverse() * quaternion.from_rotation_vector(rv1) * qs
# q2 = qs.inverse() * quaternion.from_rotation_vector(rv2) * qs
# q3 = qs.inverse() * quaternion.from_rotation_vector(rv3) * qs

x0 = [1,0,0]
xs = quaternion.rotate_vectors(qshaft,x0)
x1 = quaternion.rotate_vectors(q1,x0)
x2 = quaternion.rotate_vectors(q2,x0)
x3 = quaternion.rotate_vectors(q3,x0)
# Compare xs to vrot, should be the same
a=vrot/np.linalg.norm(vrot)
b=xs/np.linalg.norm(xs)
print('vrot_normalized=',a)
print('xs_normalized=',b)

xsm0 = quaternion.rotate_vectors(qsm_rotated,x0)

xsm = quaternion.rotate_vectors(qsm,x0)
x1m = quaternion.rotate_vectors(q1m,x0)
x2m = quaternion.rotate_vectors(q2m,x0)
x3m = quaternion.rotate_vectors(q3m,x0)

plt.figure()
ax = plt.axes(projection='3d')

DV = quaternion.rotate_vectors(qsm,dv)
plt.quiver(0,0,0,DV[0],DV[1],DV[2],color='k')

plt.quiver(0,0,0,xs[0],xs[1],xs[2],color='r')
plt.quiver(0,0,0,x1[0],x1[1],x1[2],color='r')
plt.quiver(0,0,0,x2[0],x2[1],x2[2],color='r')
plt.quiver(0,0,0,x3[0],x3[1],x3[2],color='r')

plt.quiver(0,0,0,xsm0[0],xsm0[1],xsm0[2],color='g',linewidth=3)
plt.quiver(0,0,0,xsm[0],xsm[1],xsm[2],color='b')
plt.quiver(0,0,0,x1m[0],x1m[1],x1m[2],color='b')
plt.quiver(0,0,0,x2m[0],x2m[1],x2m[2],color='b')
plt.quiver(0,0,0,x3m[0],x3m[1],x3m[2],color='b')

ax.text(x1[0],x1[1],x1[2],s="x1")
ax.text(x2[0],x2[1],x2[2],s='x2')
ax.text(x3[0],x3[1],x3[2],s='x3')

ax.set_aspect('equal','box')
lim=1
ax.set_xlim3d([-lim,lim])
ax.set_ylim3d([-lim,lim])
ax.set_zlim3d([-lim,lim])
 
plt.show(False)
print()